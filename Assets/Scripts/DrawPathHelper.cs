﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DrawPathHelper : MonoBehaviour {
	
	private Transform[] positionList;
	private List<Vector3> vectList;


	public List<Vector3> Nodes{
		get{
			initComponent();
			return vectList;
		}
	}

	void initComponent(){
		positionList = this.GetComponentsInChildren<Transform>();

		vectList = new List<Vector3>();
		for(int i = 1 ; i < positionList.Length ; i++){
			vectList.Add(positionList[i].position);
		}
	}

	void Start(){
		//initComponent();
	}

	void OnDrawGizmos(){
		initComponent();

		Gizmos.color = Color.cyan;

		if(vectList.Count >= 2){
			for(int i = 1 ; i < vectList.Count ; i++){
				Gizmos.DrawLine((Vector3)vectList[i-1], (Vector3)vectList[i]);
			}
		}

	}
}
