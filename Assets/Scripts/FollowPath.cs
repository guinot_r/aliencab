﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public abstract class FollowPath : MonoBehaviour {

	public DrawPathHelper path;
	public float speed;
	public int startNode;

	protected Vector3 firstNode;
	protected Vector3 nextNode;
	protected int indiceNode;

	protected List<Vector3> Nodes;
	protected Vector3 nextPosition;

	// Use this for initialization
	void Start () {
		Nodes = new List<Vector3>();
		Nodes = path.Nodes;
		indiceNode = startNode;
		firstNode = Nodes[indiceNode];
		nextNode = Nodes[indiceNode + 1];
		this.transform.position = new Vector3(firstNode.x, firstNode.y, this.transform.position.z);

	}

    #region useless code portion
    private float linearFunctionFromSegment(float x, Vector3 v, Vector3 a){
		return ((v.y/v.x)*x) + (a.y - ((v.y/v.x)*a.x));
	}
    #endregion

    private bool pointInSegment(){
		if(Vector2.Distance(this.transform.position, this.nextPosition) >= Vector2.Distance(this.transform.position, this.nextNode)){
			return true;
		}else{
			return false;
		}
	}

    /// <summary>
    /// this function have to be overrided in the subclasses
    /// is called when the last node is going to be reach in the next frame
    /// </summary>
    public abstract void ReachEndOfPath(Vector3 dist);

    private void InvokeRestart()
    {
        ManagerSingleton.Instance.restart();
    }

	// Update is called once per frame
	void Update () {
		if(ManagerSingleton.Instance._gameState != gameState.Lose)
        {
			Vector3 dist = Vector3.Normalize(new Vector3(nextNode.x - firstNode.x,
			                                             nextNode.y - firstNode.y,
			                                             0));

			this.nextPosition = this.transform.position + (dist * speed * Time.deltaTime);

			if(pointInSegment() == false){//si en interpolant on ne trouve pas le prochain noeud
				//on affecte tranquillement la nouvelle position
				this.transform.position = nextPosition;
			}else{//si on trouve le point en interpolant

				//si ce n'est pas la derniere node
				if(indiceNode + 2 < Nodes.Count){
					//on calcule le chemin déja parcouru sur la prochaine node
					float distanceTotale = speed * Time.deltaTime;
					float vraiDistance = Vector2.Distance((Vector2)this.transform.position, (Vector2)nextNode);
					this.transform.position = new Vector3(nextNode.x, nextNode.y, this.transform.position.z);
					indiceNode++; // on passe a la prochaine node
					firstNode = Nodes[indiceNode];
					nextNode = Nodes[indiceNode + 1];

					dist = Vector3.Normalize(new Vector3(nextNode.x - firstNode.x,
					                                     nextNode.y - firstNode.y,
					                                     0));

					this.transform.position = this.transform.position + (dist * (distanceTotale - vraiDistance));
				}
                else if (indiceNode + 2 == Nodes.Count) //si on arrive a la derniere node:
                {
                    ReachEndOfPath(dist);
					/*if(ManagerSingleton.Instance._gameType == gameType.Level){//si on est en mode level simple
						ManagerSingleton.Instance.playerWin();
                        Invoke("InvokeRestart", 8);
					}else{//si on est en mode endless
						this.path = ManagerSingleton.Instance.getNextRoad();
						Nodes = new List<Vector3>(path.Nodes);
						float distanceTotale = speed * Time.deltaTime;
						float vraiDistance = Vector2.Distance((Vector2)this.transform.position, (Vector2)nextNode);
						indiceNode = 0;
						firstNode = Nodes[indiceNode];
						nextNode = Nodes[indiceNode + 1];
						this.transform.position = new Vector3(firstNode.x, firstNode.y, this.transform.position.z);
						
						dist = Vector3.Normalize(new Vector3(nextNode.x - firstNode.x,
						                                     nextNode.y - firstNode.y,
						                                     0));
						
						this.transform.position = this.transform.position + (dist * (distanceTotale - vraiDistance));
					}*/
					
				}
			}

		}

	}
}
