using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CreateRoads : MonoBehaviour {


	/// <summary>
	/// On met tous les patterns de route dans cette liste
	/// </summary>
	public List<RouteDefinition> roadList;
	public int numberOfRoads;

	//ce tableau sert a stocker le nombre différent type de route pondérer trier par cardinalité d'entrer
	private int[] statList;
	// Use this for initialization
	void Start () {
        ManagerSingleton.Instance.createRoadObj = this;
		statList = new int[4];
		for(int i = 0 ; i<4 ; i++)
			statList[i] = 0;

		foreach(RouteDefinition rd in roadList)
			statList[(int)rd.entryCardinality]++;

		for(int i = 0 ; i <5 ; i++)
			createNewRoad();
	}

	public void createNewRoad(){
		RouteDefinition[] rds = this.gameObject.GetComponentsInChildren<RouteDefinition>();
		RouteDefinition lastPoint = rds[rds.Length - 1];

		DrawPathHelper lastPath = lastPoint.GetComponentInChildren<DrawPathHelper>();
		Vector3 lastPos = lastPath.Nodes[lastPath.Nodes.Count - 1];


		Card neededRoadType = Card.Est;

		switch(lastPoint.exitCardinality){
		case Card.Est:
			neededRoadType = Card.Ouest;
			break;
		case Card.Nord:
			neededRoadType = Card.Sud;
			break;
		case Card.Ouest:
			neededRoadType = Card.Est;
			break;
		case Card.Sud:
			neededRoadType = Card.Nord;
			break;
		}

		int counter = 0;
		foreach(RouteDefinition rd in roadList){
			if(rd.entryCardinality == neededRoadType){
				counter++;
				if(counter == statList[(int)neededRoadType]){
					RouteDefinition rdtmp =(RouteDefinition) Instantiate(rd, (Vector3)lastPos, Quaternion.identity);
					rdtmp.transform.parent = this.transform;
					rdtmp.name = numberOfRoads.ToString();
					numberOfRoads++;
					break;
				}else{
					int rand = (int) Random.Range(1, statList[(int)neededRoadType]);
					if(rand == 1){
						RouteDefinition rdtmp =(RouteDefinition) Instantiate(rd, (Vector3)lastPos, Quaternion.identity);
						rdtmp.transform.parent = this.transform;
						rdtmp.name = numberOfRoads.ToString();
						numberOfRoads++;
						break;
					}
				}

			}
		}
	}
	

	// Update is called once per frame
	void Update () {
	}
}
