﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MenuFollowPath : FollowPath {

    public override void ReachEndOfPath(Vector3 dist)
    {
        float distanceTotale = speed * Time.deltaTime;
        float vraiDistance = Vector2.Distance((Vector2)this.transform.position, (Vector2)nextNode);
        // On indique juste que l'on repasse a la premiere node pour boucler.
        indiceNode = 0;
        firstNode = Nodes[indiceNode];
        nextNode = Nodes[indiceNode + 1];
        this.transform.position = new Vector3(firstNode.x, firstNode.y, this.transform.position.z);

        dist = Vector3.Normalize(new Vector3(nextNode.x - firstNode.x,
                                                nextNode.y - firstNode.y,
                                                0));

        this.transform.position = this.transform.position + (dist * (distanceTotale - vraiDistance));
    }
}
