﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TaxiFollowPath : FollowPath {

    public override void ReachEndOfPath(Vector3 dist)
    {
        // Si on est en mode level simple.
        if (ManagerSingleton.Instance._gameType == gameType.Level)
        {
            ManagerSingleton.Instance.playerWin();
            Invoke("InvokeRestart", 8);
        }
        // Si on est en mode endless.
        else
        {
            this.path = ManagerSingleton.Instance.getNextRoad();
            Nodes = new List<Vector3>(path.Nodes);
            float distanceTotale = speed * Time.deltaTime;
            float vraiDistance = Vector2.Distance((Vector2)this.transform.position, (Vector2)nextNode);
            indiceNode = 0;
            firstNode = Nodes[indiceNode];
            nextNode = Nodes[indiceNode + 1];
            this.transform.position = new Vector3(firstNode.x, firstNode.y, this.transform.position.z);

            dist = Vector3.Normalize(new Vector3(nextNode.x - firstNode.x,
                                                 nextNode.y - firstNode.y,
                                                 0));

            this.transform.position = this.transform.position + (dist * (distanceTotale - vraiDistance));
        }
    }
}
