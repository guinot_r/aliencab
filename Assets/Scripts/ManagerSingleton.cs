﻿using UnityEngine;
using System.Collections;

#region Enumerations
public enum gameState
{
    Menu,
    Loading,
    Starting,
    Playing,
    Lose,
    Win
}

public enum gameType
{
    Level,
    Endless
}

public enum TypePath
{
    SN,
    SE,
    SO,
    EN,
    EO,
    OE,
    ON

}
#endregion

public class ManagerSingleton
{

    #region Singleton Implementation
    private static ManagerSingleton _instance;
    private static readonly object lockInstance = new object();

    public static ManagerSingleton Instance
    {
        get
        {
            if (_instance == null)
            {
                lock (lockInstance)
                {
                    _instance = new ManagerSingleton();
                }
            }
            return _instance;
        }
    }

    private ManagerSingleton() { }
    #endregion

    public gameType             _gameType = gameType.Endless;
    public gameState            _gameState = gameState.Menu;
    public Animator             tentacule_animator;
    public ManageCarburant      carbu;
    public Animator             taxi_animator;
    public AudioSource          miam, lose, bg;
    public CreateRoads          createRoadObj;
    public int                  currentRoadNumber = 0;

    private bool                _isFirstUpdateCarbu = true;
    private bool                _isBoost;

    public void fouette()
    {
        tentacule_animator.Play("Tentacule_Fouet");
    }

    public void updateCarburant()
    {

        tentacule_animator.Play("Tentacule_Attrape");
        taxi_animator.Play("Taxi_Bonus");
        miam.PlayOneShot(miam.clip);
        carbu.augmentation();

    }
    public void setIsBoost(bool b)
    {
        _isBoost = b;
    }
    public bool getIsCarbuMax()
    {
        if (_isFirstUpdateCarbu)
        {
            _isFirstUpdateCarbu = false;
            return false;
        }
        return carbu.getIsMax();
    }

    public DrawPathHelper getNextRoad()
    {
        createRoadObj.createNewRoad();
        if (currentRoadNumber > 10)
        {
            createRoadObj.GetComponentsInChildren<DrawPathHelper>()[currentRoadNumber - 10].gameObject.SetActive(false);
        }
        else
        {
            currentRoadNumber++;
        }
        DrawPathHelper dph = createRoadObj.GetComponentsInChildren<DrawPathHelper>()[currentRoadNumber];
        return dph;
    }

    public void playerLose()
    {
        if (_isBoost == false)
        {
            tentacule_animator.Play("Tentacule_Die");
            taxi_animator.Play("Taxi_Dead");
            _gameState = gameState.Lose;
            lose.PlayOneShot(lose.clip);
            bg.mute = true;
            //Invoke("restart", 2);
        }
    }
    public void playerWin()
    {
        //animation fusée !
        _gameState = gameState.Win;
        tentacule_animator.gameObject.SetActive(false);
        taxi_animator.gameObject.SetActive(false);
        //Invoke("restart", 8);
    }

    public void restart()
    {
        if (_gameType == gameType.Endless)
        {
            Application.LoadLevel("Test Endless");
            currentRoadNumber = 0;
        }
        else if (_gameType == gameType.Level)
        {
            Application.LoadLevel("LevelOne");
        }
        
        _gameState = gameState.Menu; // valeur transitionnelle
    }

}
