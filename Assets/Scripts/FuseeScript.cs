﻿using UnityEngine;
using System.Collections;

public class FuseeScript : MonoBehaviour {

	private bool firstTime;
	private ParticleSystem[] PSs;

	private float counter;
	// Use this for initialization
	void Start () {
		firstTime = true;
		PSs = this.gameObject.GetComponentsInChildren<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () {
		if(ManagerSingleton.Instance._gameState == gameState.Win){
			if(firstTime){
				firstTime = false;
				foreach(ParticleSystem ps in PSs){
					ps.Play();
					ps.loop = true;
				}
			}
			counter+=Time.deltaTime;
			//exp(x-1)-exp(-1)
			this.transform.Translate(new Vector3(0, Mathf.Exp(counter - 8 ) - Mathf.Exp(-8), 0));

		}
	}
}
