﻿using UnityEngine;
using System.Collections;

public class ObstacleGizmos : MonoBehaviour {

	void OnDrawGizmos(){

        Gizmos.matrix = transform.localToWorldMatrix;
		Gizmos.color = Color.Lerp(Color.red, Color.white, 0.3f);
		Gizmos.DrawCube(Vector3.zero, new Vector3(0.8f, 4f, 1f));

	}
}
