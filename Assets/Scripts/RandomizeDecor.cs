﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RandomizeDecor : MonoBehaviour {
	public List<Sprite> lsp;



	public float Width;
	public float Height;
	public int NbBuildings;
	// Use this for initialization
	void Start () {
		int NbSprites = lsp.Count;
		float PosX, PosY;
		PosX = this.transform.position.x - Camera.main.orthographicSize;
		PosY = this.transform.position.y - Camera.main.orthographicSize;
		for (int i = 0; i < NbBuildings; i++) 
		{
			float newX, newY;
			newX = Random.Range(PosX, PosX+Width);
			newY = Random.Range(PosY, PosY+Height);
			GameObject building = (GameObject)Instantiate(Resources.Load("Decor"),new Vector3(newX, newY, 0), Quaternion.identity);
			int texture = Random.Range(0, NbSprites);
			building.AddComponent<SpriteRenderer>();
			building.GetComponent<SpriteRenderer>().sprite = lsp[texture];
			building.GetComponent<SpriteRenderer>().sortingOrder = (int)(-newY-1000);
		}
	}

	void OnDrawGizmos(){
		Gizmos.color = Color.gray;
		Gizmos.DrawCube(this.transform.position, new Vector3(Width, Height, 1));
	}
}
