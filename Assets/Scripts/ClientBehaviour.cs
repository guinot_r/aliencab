﻿using UnityEngine;
using System.Collections;

public class ClientBehaviour : MonoBehaviour {

	private bool IsFall;
	private bool IsCliked;
	private bool IsDead;

	private Animator anim;
	private GameObject taxi;
	private ParticleSystem ps;
	private float counter;

	// Use this for initialization
	void Start () {
		IsDead = false;
		IsFall = false;
		IsCliked = false;
		ps = this.GetComponentInChildren<ParticleSystem>();
		taxi = GameObject.Find("TaxiTentacule");
		anim = this.GetComponentInChildren<Animator>();
		counter = 0;
	}

	public void setFall(bool b){
		this.IsFall = b;
		if(this.tag == "ClientA"){
			anim.Play("ClientA_Fall");
		}else if(this.tag == "ClientB"){
			anim.Play("ClientB_Fall");
		}else if(this.tag == "ClientC"){
			anim.Play("ClientC_Fall");
		}else{
			anim.Play("ClientD_Fall");
		}
	}

	public void setCliked(bool b){
		this.IsCliked = b;
	}


	void thisDie(){
		this.GetComponentInChildren<SpriteRenderer>().enabled = false;
		enabled = false;
		Destroy(this.gameObject);
	}

	void explode(){
		ps.Play();
	}

	// Update is called once per frame
	void Update () {
		if(IsFall && IsCliked){
			//31 le nombre de frame d'annimation de l'attrape
			counter+= Time.deltaTime;
			float CurrentDistance = Vector2.Distance(taxi.transform.position, this.transform.position);
			Vector3 NormalisedVector = Vector3.Normalize(new Vector3((taxi.transform.position.x - this.transform.position.x),
			                                                         (taxi.transform.position.y - this.transform.position.y),
			                                                         0));
			this.transform.position = this.transform.position + (NormalisedVector * counter * CurrentDistance);

			if(IsDead == false){
				IsDead = true;
				Invoke("explode", 0.5f);
				Invoke("thisDie", 1);
			}
		}
	}

    public void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(this.transform.position, 0.4f);
    }

}
