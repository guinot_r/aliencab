﻿using UnityEngine;
using System.Collections;

public class ManageBoutons : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
	
	}

	void OnClick()
	{
        ManagerSingleton.Instance._gameState = gameState.Menu;
        ManagerSingleton.Instance.currentRoadNumber = 0;
		switch(this.tag)
		{
		case "btnJouer":
                ManagerSingleton.Instance._gameType = gameType.Level;
                Application.LoadLevel("LevelOne");
			break;
        case "btnEndLess":
                ManagerSingleton.Instance._gameType = gameType.Endless;
                Application.LoadLevel("Test Endless");
            break;
		case "btnCred":
                Application.LoadLevel("SceneCredits");
			break;
        case "btnBack":
                Application.LoadLevel("Menu principal");
            break;
		case "btnLeave":

            #if !UNITY_EDITOR
			    Application.Quit();
            #else
                UnityEditor.EditorApplication.isPlaying = false;
            #endif
			break;
		}

			
	}
}
