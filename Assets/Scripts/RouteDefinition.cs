﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum Card : int{
	Nord = 0,
	Est = 1,
	Ouest = 2,
	Sud = 3
}

public class RouteDefinition : MonoBehaviour {



	public Card entryCardinality;
	public Card exitCardinality;

	public Vector3 lastRoadPosition;

	// Use this for initialization
	void Start () {
		List<Vector3> tmpL = this.gameObject.GetComponentInChildren<DrawPathHelper>().Nodes;
		lastRoadPosition = tmpL[tmpL.Count - 1];
	}
}
