﻿using UnityEngine;
using System.Collections;

public class ManageCarburant : MonoBehaviour {
	public float diminutionParSec;
	public float augmentationBonus;

	private UISlider slider;
	// Use this for initialization
	void Start () {
        ManagerSingleton.Instance.carbu = this;
		slider = this.gameObject.GetComponent<UISlider> ();
	}

	public void augmentation(){
		if(slider != null)
			slider.sliderValue += augmentationBonus;
	}
	public bool getIsMax()
	{
		if (slider.sliderValue == 1F)
			return true;
		else
			return false;
	}

    private void InvokeRestart()
    {
        ManagerSingleton.Instance.restart();
    }

	// Update is called once per frame
	void Update () {
        if (ManagerSingleton.Instance._gameState != gameState.Lose && ManagerSingleton.Instance._gameState != gameState.Win)
        {
			if(slider!=null){
				slider.sliderValue -= diminutionParSec * Time.deltaTime;
				if(slider.sliderValue == 0){
                    ManagerSingleton.Instance.playerLose();
                    Invoke("InvokeRestart", 2);
				}
			}
		}
	}

}
