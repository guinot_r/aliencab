﻿using UnityEngine;
using System.Collections;

public class SwapSide : MonoBehaviour {

	private float _lastX = 0;
    private float xScale;

	// Use this for initialization
	void Start () {
        xScale = transform.localScale.x;
	}
	
	// Update is called once per frame
	void Update () {
		if (this.transform.position.x >= _lastX) {
						this.gameObject.transform.localScale = new Vector3 (xScale, this.gameObject.transform.localScale.y, this.gameObject.transform.localScale.z);
		} else {
			this.gameObject.transform.localScale = new Vector3(-xScale, this.gameObject.transform.localScale.y, this.gameObject.transform.localScale.z);

		}
		_lastX = this.transform.position.x;
	}
}
