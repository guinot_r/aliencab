﻿using UnityEngine;
using System.Collections;

public class HitBox_tentacule_script : MonoBehaviour {
	//public ManagerScripts MS;

	private bool mousePressed;
	private bool isThisFrameCliked;


	// Use this for initialization
	void Start () {
        ManagerSingleton.Instance.tentacule_animator = this.gameObject.GetComponent<Animator>();
		acquireMouseState();
		isThisFrameCliked = false;
	}

	public void acquireMouseState(){
		if(Input.GetMouseButtonDown(0)){
			mousePressed = true;
		}else if(Input.GetMouseButtonUp(0)){
			mousePressed = false;
		}
	}



	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonDown(0) && !mousePressed){
			isThisFrameCliked = true;
		}else{
			isThisFrameCliked = false;
		}
		acquireMouseState();
	}

    private void InvokeRestart()
    {
        ManagerSingleton.Instance.restart();
    }

	void OnTriggerEnter2D(Collider2D other){
		if(ManagerSingleton.Instance._gameState != gameState.Lose){
			if((other.tag == "ClientA" || other.tag == "ClientB" || other.tag == "ClientC" || other.tag == "ClientD") && ManagerSingleton.Instance._gameState != gameState.Lose){
				other.gameObject.GetComponent<ClientBehaviour>().setFall(true);
				ManagerSingleton.Instance.fouette();
			}

			if(other.tag == "Obstacle" && ManagerSingleton.Instance._gameState != gameState.Lose){
				ManagerSingleton.Instance.playerLose();
                Invoke("InvokeRestart", 2);
			}
		}
	}

	void OnTriggerStay2D(Collider2D other){
        //on utilise joystick names pour savoir le nombre de joystick brancher a l'ordinateur
        if (Input.GetJoystickNames().Length > 0){//si le joystick est activer
			if(Input.GetKey(KeyCode.JoystickButton0) && (other.tag == "ClientA" || other.tag == "ClientB" || other.tag == "ClientC" || other.tag == "ClientD") && ManagerSingleton.Instance._gameState != gameState.Lose){
				ManagerSingleton.Instance.updateCarburant();
				if(other.gameObject.GetComponent<ClientBehaviour>() != null)
				other.gameObject.GetComponent<ClientBehaviour>().setCliked(true);
			}
		}else{//avec la souris
			if(isThisFrameCliked && (other.tag == "ClientA" || other.tag == "ClientB" || other.tag == "ClientC" || other.tag == "ClientD") && ManagerSingleton.Instance._gameState != gameState.Lose){
				ManagerSingleton.Instance.updateCarburant();
				if(other.gameObject.GetComponent<ClientBehaviour>() != null)
				other.gameObject.GetComponent<ClientBehaviour>().setCliked(true);
			}
		}
	}
}
