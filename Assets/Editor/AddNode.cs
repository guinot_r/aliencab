﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(DrawPathHelper))]
public class AddNode : Editor {

	public override void OnInspectorGUI ()
	{
		if(GUILayout.Button("Add Node")){
			GameObject obj = (GameObject)Editor.Instantiate(Resources.Load("Node"), ((DrawPathHelper)target).transform.position , Quaternion.identity);
			obj.name = "Way Point";
			obj.transform.parent = ((DrawPathHelper)target).transform;
		}
	}

}
